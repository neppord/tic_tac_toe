#include <stdio.h>
#define new_board() (struct board) {{0, 0, 0, 0, 0, 0, 0, 0, 0}}

#define EMPTY_CELL 0
#define PLAYER_ONE 1
#define PLAYER_TWO 2


struct board {
  short cells[9];
};

struct text_board {
  char string[41];
};

struct board board_make_move(struct board board, short cell_nr, short marker){
  board.cells[cell_nr] = marker;
  return board;
} 

struct text_board board_to_string(struct board board) {
  struct text_board text_board;
  sprintf(
    text_board.string,
    "|%c|%c|%c|\n+-+-+-+\n|%c|%c|%c|\n+-+-+-+\n|%c|%c|%c|\n",
    board.cells[0] == EMPTY_CELL? '0': (board.cells[0] == PLAYER_ONE? 'X': 'O'),
    board.cells[1] == EMPTY_CELL? '1': (board.cells[1] == PLAYER_ONE? 'X': 'O'),
    board.cells[2] == EMPTY_CELL? '2': (board.cells[2] == PLAYER_ONE? 'X': 'O'),
    board.cells[3] == EMPTY_CELL? '3': (board.cells[3] == PLAYER_ONE? 'X': 'O'),
    board.cells[4] == EMPTY_CELL? '4': (board.cells[4] == PLAYER_ONE? 'X': 'O'),
    board.cells[5] == EMPTY_CELL? '5': (board.cells[5] == PLAYER_ONE? 'X': 'O'),
    board.cells[6] == EMPTY_CELL? '6': (board.cells[6] == PLAYER_ONE? 'X': 'O'),
    board.cells[7] == EMPTY_CELL? '7': (board.cells[7] == PLAYER_ONE? 'X': 'O'),
    board.cells[8] == EMPTY_CELL? '8': (board.cells[8] == PLAYER_ONE? 'X': 'O')
  );
  return text_board;
}
