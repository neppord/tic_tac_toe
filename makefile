build: hello_world.c
	clang hello_world.c

run: build
	./a.out

test: test.c test_board.c board.c
	clang test.c -o run_tests
	./run_tests
