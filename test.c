#include "test_board.c"
#include "test_moves.c"
#include <stdio.h>

int main(int argc, char *argv[]){
  test_board();
  test_moves();
  printf("All tests passed!");
  return 0;
}
