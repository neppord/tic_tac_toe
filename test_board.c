#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "board.c"

void test_board_make_move_single() {
  struct board empty_board = new_board();
  struct board board_with_move;
  struct board board_with_two_moves;

  board_with_move = board_make_move(empty_board, 0, PLAYER_ONE);
  
  assert(board_with_move.cells[0] == PLAYER_ONE);
  assert(empty_board.cells[0] == EMPTY_CELL);

}

void test_board_make_move_double(){
  struct board empty_board;
  struct board board_with_move;
  struct board board_with_two_moves;

  board_with_move = board_make_move(empty_board, 0, PLAYER_ONE);
  board_with_two_moves = board_make_move(board_with_move, 5, PLAYER_TWO);

  assert(board_with_two_moves.cells[5] == PLAYER_TWO);
}

void test_board_make_move(){
  test_board_make_move_single();
  test_board_make_move_double();
}

void test_board_to_string_empty_board(){
  struct text_board board = board_to_string(new_board());
  assert(strcmp(board.string, "|0|1|2|\n+-+-+-+\n|3|4|5|\n+-+-+-+\n|6|7|8|\n") == 0);
}

void test_board_to_string_board_with_one_player(){
  struct text_board text_board;
  struct board board = board_make_move(new_board(), 0, PLAYER_ONE);
  text_board = board_to_string(board);
  assert(strcmp(text_board.string, "|X|1|2|\n+-+-+-+\n|3|4|5|\n+-+-+-+\n|6|7|8|\n") == 0);
}

void test_board_to_string_multiple_moves(){
  struct text_board text_board;
  struct board board = board_make_move(new_board(), 0, PLAYER_ONE);
  board = board_make_move(board, 2, PLAYER_TWO);
  board = board_make_move(board, 4, PLAYER_ONE);
  board = board_make_move(board, 5, PLAYER_ONE);
  board = board_make_move(board, 8, PLAYER_TWO);
  text_board = board_to_string(board);
  assert(strcmp(text_board.string, "|X|1|O|\n+-+-+-+\n|3|X|X|\n+-+-+-+\n|6|7|O|\n") == 0);
}

void test_board_to_string(){
  test_board_to_string_empty_board();
  test_board_to_string_board_with_one_player();
  test_board_to_string_multiple_moves();
}

void test_board(){
  test_board_make_move();
  test_board_to_string();
}
